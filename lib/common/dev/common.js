if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'common'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'common'.");
}
var common = function (_, Kotlin) {
  'use strict';
  var trimIndent = Kotlin.kotlin.text.trimIndent_pdl1vz$;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var Unit = Kotlin.kotlin.Unit;
  var throwCCE = Kotlin.throwCCE;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var getCallableRef = Kotlin.getCallableRef;
  var math = Kotlin.kotlin.math;
  var Enum = Kotlin.kotlin.Enum;
  var throwISE = Kotlin.throwISE;
  var toString = Kotlin.toString;
  var Exception_init = Kotlin.kotlin.Exception_init_pdl1vj$;
  var ensureNotNull = Kotlin.ensureNotNull;
  FullGame.prototype = Object.create(Loop.prototype);
  FullGame.prototype.constructor = FullGame;
  Shader$Type.prototype = Object.create(Enum.prototype);
  Shader$Type.prototype.constructor = Shader$Type;
  Mat4Color.prototype = Object.create(Shader.prototype);
  Mat4Color.prototype.constructor = Mat4Color;
  Mat4_0.prototype = Object.create(Shader.prototype);
  Mat4_0.prototype.constructor = Mat4_0;
  GLX$ShaderType.prototype = Object.create(Enum.prototype);
  GLX$ShaderType.prototype.constructor = GLX$ShaderType;
  function vert() {
    vert_instance = this;
    this.mat4 = trimIndent('\nattribute vec4 a_position;\nuniform mat4 u_matrix;\n\nvoid main() {\n  gl_Position = u_matrix * a_position;\n}\n');
    this.mat4_color = trimIndent('\nattribute vec4 a_position;\nattribute vec4 a_color;\n\nuniform mat4 u_matrix;\n\nvarying vec4 v_color;\n\nvoid main() {\n  gl_Position = u_matrix * a_position;\n  v_color = a_color;\n}\n');
    this.noop = trimIndent('\nattribute vec4 aVertexPosition;\n\nvoid main() {\n  gl_Position = aVertexPosition;\n}\n');
  }
  vert.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'vert',
    interfaces: []
  };
  var vert_instance = null;
  function vert_getInstance() {
    if (vert_instance === null) {
      new vert();
    }
    return vert_instance;
  }
  function frag() {
    frag_instance = this;
    this.uniform = trimIndent('\nprecision mediump float;\n\nuniform vec4 u_color;\n\nvoid main() {\n    gl_FragColor = u_color;\n}\n\n');
    this.varying = trimIndent('\nprecision mediump float;\n\nvarying vec4 v_color;\n\nvoid main() {\n    gl_FragColor = v_color;\n}\n\n');
  }
  frag.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'frag',
    interfaces: []
  };
  var frag_instance = null;
  function frag_getInstance() {
    if (frag_instance === null) {
      new frag();
    }
    return frag_instance;
  }
  function FullGame(canvas) {
    Loop.call(this);
    this.canvas = canvas;
    this.gl = this.canvas.getContext('webgl');
    this.glx = new GLX(this.gl);
    this.touchDownX_ug1ezz$_0 = 0;
    this.touchDownY_ug1f0u$_0 = 0;
    this.dragging_5142jb$_0 = false;
  }
  FullGame.prototype.doResize = function () {
    var width = this.canvas.clientWidth;
    var height = this.canvas.clientHeight;
    this.canvas.width = width;
    this.canvas.height = height;
    this.gl.viewport(0, 0, this.gl.drawingBufferWidth, this.gl.drawingBufferHeight);
    this.draw();
  };
  Object.defineProperty(FullGame.prototype, 'touchDownX', {
    get: function () {
      return this.touchDownX_ug1ezz$_0;
    },
    set: function (touchDownX) {
      this.touchDownX_ug1ezz$_0 = touchDownX;
    }
  });
  Object.defineProperty(FullGame.prototype, 'touchDownY', {
    get: function () {
      return this.touchDownY_ug1f0u$_0;
    },
    set: function (touchDownY) {
      this.touchDownY_ug1f0u$_0 = touchDownY;
    }
  });
  Object.defineProperty(FullGame.prototype, 'dragging', {
    get: function () {
      return this.dragging_5142jb$_0;
    },
    set: function (dragging) {
      this.dragging_5142jb$_0 = dragging;
    }
  });
  function FullGame$run$lambda(this$FullGame) {
    return function (it) {
      this$FullGame.doResize();
      return Unit;
    };
  }
  function FullGame$run$lambda_0(this$FullGame) {
    return function (it) {
      var tmp$;
      var evt = Kotlin.isType(tmp$ = it, MouseEvent) ? tmp$ : throwCCE();
      this$FullGame.dragging = false;
      this$FullGame.touchUp_vux9f0$(evt.clientX, evt.clientY);
      return null;
    };
  }
  function FullGame$run$lambda_1(this$FullGame) {
    return function (it) {
      var tmp$;
      var evt = Kotlin.isType(tmp$ = it, MouseEvent) ? tmp$ : throwCCE();
      this$FullGame.touchDownX = it.clientX;
      this$FullGame.touchDownY = it.clientY;
      this$FullGame.dragging = true;
      this$FullGame.touchDown();
      return null;
    };
  }
  function FullGame$run$lambda_2(this$FullGame) {
    return function (it) {
      var tmp$;
      if (this$FullGame.dragging) {
        var evt = Kotlin.isType(tmp$ = it, MouseEvent) ? tmp$ : throwCCE();
        console.dir(evt);
        this$FullGame.drag_vux9f0$(evt.clientX, evt.clientY);
      }
      return Unit;
    };
  }
  FullGame.prototype.run = function () {
    window.onresize = FullGame$run$lambda(this);
    this.doResize();
    this.canvas.onmouseup = FullGame$run$lambda_0(this);
    this.canvas.onmousedown = FullGame$run$lambda_1(this);
    this.canvas.onmousemove = FullGame$run$lambda_2(this);
    Loop.prototype.run.call(this);
  };
  FullGame.prototype.touchDown = function () {
  };
  FullGame.prototype.touchUp_vux9f0$ = function (x, y) {
  };
  FullGame.prototype.drag_vux9f0$ = function (x, y) {
  };
  FullGame.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'FullGame',
    interfaces: [Loop]
  };
  function FullGame_init(id, $this) {
    if (id === void 0)
      id = '#glCanvas';
    $this = $this || Object.create(FullGame.prototype);
    var tmp$;
    FullGame.call($this, Kotlin.isType(tmp$ = document.querySelector(id), HTMLCanvasElement) ? tmp$ : throwCCE());
    return $this;
  }
  function Loop() {
    this.timeStep = 1000.0 / 60;
    this.lastFrameTs_7hp69h$_0 = 0.0;
    this.actualTs_2ghjes$_0 = 0.0;
    this.delta_oqgrcd$_0 = 0.0;
  }
  Object.defineProperty(Loop.prototype, 'lastFrameTs', {
    get: function () {
      return this.lastFrameTs_7hp69h$_0;
    },
    set: function (lastFrameTs) {
      this.lastFrameTs_7hp69h$_0 = lastFrameTs;
    }
  });
  Object.defineProperty(Loop.prototype, 'actualTs', {
    get: function () {
      return this.actualTs_2ghjes$_0;
    },
    set: function (actualTs) {
      this.actualTs_2ghjes$_0 = actualTs;
    }
  });
  Loop.prototype.mainLoop_svqqsu$_0 = function (ts) {
    this.delta_oqgrcd$_0 += ts - this.lastFrameTs;
    this.lastFrameTs = ts;
    while (this.delta_oqgrcd$_0 >= this.timeStep) {
      this.actualTs = this.actualTs + this.timeStep;
      this.step();
      this.delta_oqgrcd$_0 -= this.timeStep;
    }
    this.draw();
    window.requestAnimationFrame(getCallableRef('mainLoop', function ($receiver, ts) {
      return $receiver.mainLoop_svqqsu$_0(ts), Unit;
    }.bind(null, this)));
  };
  Loop.prototype.run = function () {
    window.requestAnimationFrame(getCallableRef('mainLoop', function ($receiver, ts) {
      return $receiver.mainLoop_svqqsu$_0(ts), Unit;
    }.bind(null, this)));
  };
  Loop.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Loop',
    interfaces: []
  };
  function Mat4(a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34, a41, a42, a43, a44) {
    this.a11 = a11;
    this.a12 = a12;
    this.a13 = a13;
    this.a14 = a14;
    this.a21 = a21;
    this.a22 = a22;
    this.a23 = a23;
    this.a24 = a24;
    this.a31 = a31;
    this.a32 = a32;
    this.a33 = a33;
    this.a34 = a34;
    this.a41 = a41;
    this.a42 = a42;
    this.a43 = a43;
    this.a44 = a44;
  }
  Mat4.prototype.mul4x4r_8odxlg$ = function (b11, b12, b13, b14, b21, b22, b23, b24, b31, b32, b33, b34, b41, b42, b43, b44) {
    var a11 = this.a11;
    var a12 = this.a12;
    var a13 = this.a13;
    var a14 = this.a14;
    var a21 = this.a21;
    var a22 = this.a22;
    var a23 = this.a23;
    var a24 = this.a24;
    var a31 = this.a31;
    var a32 = this.a32;
    var a33 = this.a33;
    var a34 = this.a34;
    var a41 = this.a41;
    var a42 = this.a42;
    var a43 = this.a43;
    var a44 = this.a44;
    this.a11 = a11 * b11 + a12 * b21 + a13 * b31 + a14 * b41;
    this.a12 = a11 * b12 + a12 * b22 + a13 * b32 + a14 * b42;
    this.a13 = a11 * b13 + a12 * b23 + a13 * b33 + a14 * b43;
    this.a14 = a11 * b14 + a12 * b24 + a13 * b34 + a14 * b44;
    this.a21 = a21 * b11 + a22 * b21 + a23 * b31 + a24 * b41;
    this.a22 = a21 * b12 + a22 * b22 + a23 * b32 + a24 * b42;
    this.a23 = a21 * b13 + a22 * b23 + a23 * b33 + a24 * b43;
    this.a24 = a21 * b14 + a22 * b24 + a23 * b34 + a24 * b44;
    this.a31 = a31 * b11 + a32 * b21 + a33 * b31 + a34 * b41;
    this.a32 = a31 * b12 + a32 * b22 + a33 * b32 + a34 * b42;
    this.a33 = a31 * b13 + a32 * b23 + a33 * b33 + a34 * b43;
    this.a34 = a31 * b14 + a32 * b24 + a33 * b34 + a34 * b44;
    this.a41 = a41 * b11 + a42 * b21 + a43 * b31 + a44 * b41;
    this.a42 = a41 * b12 + a42 * b22 + a43 * b32 + a44 * b42;
    this.a43 = a41 * b13 + a42 * b23 + a43 * b33 + a44 * b43;
    this.a44 = a41 * b14 + a42 * b24 + a43 * b34 + a44 * b44;
    return this;
  };
  var Math_0 = Math;
  Mat4.prototype.perspective_7b5o5w$ = function (fovy, aspect, znear, zfar) {
    var x = fovy / 180 * math.PI / 2;
    var f = 1.0 / Math_0.tan(x);
    var nf = 1.0 / (zfar - znear);
    return this.mul4x4r_8odxlg$(f / aspect, 0.0, 0.0, 0.0, 0.0, f, 0.0, 0.0, 0.0, 0.0, -(zfar + znear) * nf, -2 * zfar * znear * nf, 0.0, 0.0, -1.0, 0.0);
  };
  Mat4.prototype.translate_y2kzbl$ = function (dx, dy, dz) {
    return this.mul4x4r_8odxlg$(1.0, 0.0, 0.0, dx, 0.0, 1.0, 0.0, dy, 0.0, 0.0, 1.0, dz, 0.0, 0.0, 0.0, 1.0);
  };
  Mat4.prototype.toArray = function () {
    return [this.a11, this.a21, this.a31, this.a41, this.a12, this.a22, this.a32, this.a42, this.a13, this.a23, this.a33, this.a43, this.a14, this.a24, this.a34, this.a44];
  };
  Mat4.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Mat4',
    interfaces: []
  };
  function Mat4_init($this) {
    $this = $this || Object.create(Mat4.prototype);
    Mat4.call($this, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0);
    return $this;
  }
  function Shader(gl, vert, frag) {
    var tmp$;
    var loadShader = Shader$program$lambda$loadShader(gl);
    var vertexShader = loadShader(WebGLRenderingContext.VERTEX_SHADER, vert);
    var fragmentShader = loadShader(WebGLRenderingContext.FRAGMENT_SHADER, frag);
    var shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);
    if (!(typeof (tmp$ = gl.getProgramParameter(shaderProgram, WebGLRenderingContext.LINK_STATUS)) === 'boolean' ? tmp$ : throwCCE())) {
      throw Exception_init('Error linking program: ' + toString(gl.getProgramInfoLog(shaderProgram)));
    }
    this.program = shaderProgram;
  }
  Shader.prototype.attribute_61zpoe$ = function (name) {
    return new Shader$Attribute(this, name);
  };
  Shader.prototype.uniform_61zpoe$ = function (name) {
    return new Shader$Uniform(this, name);
  };
  function Shader$Attribute($outer, name) {
    this.$outer = $outer;
    this.name = name;
  }
  Shader$Attribute.prototype.vec4 = function () {
    return new Shader$TypedAttribute(this.$outer, this, Shader$Type$VEC4_getInstance());
  };
  Shader$Attribute.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Attribute',
    interfaces: []
  };
  function Shader$TypedAttribute($outer, attribute, type) {
    this.$outer = $outer;
  }
  Shader$TypedAttribute.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'TypedAttribute',
    interfaces: []
  };
  function Shader$Uniform($outer, name) {
    this.$outer = $outer;
    this.name = name;
  }
  Shader$Uniform.prototype.vec4 = function () {
    return new Shader$TypedUniform(this.$outer, this, Shader$Type$VEC4_getInstance());
  };
  Shader$Uniform.prototype.mat4 = function () {
    return new Shader$TypedUniform(this.$outer, this, Shader$Type$MAT4_getInstance());
  };
  Shader$Uniform.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Uniform',
    interfaces: []
  };
  function Shader$TypedUniform($outer, attribute, type) {
    this.$outer = $outer;
  }
  Shader$TypedUniform.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'TypedUniform',
    interfaces: []
  };
  function Shader$Type(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function Shader$Type_initFields() {
    Shader$Type_initFields = function () {
    };
    Shader$Type$VEC4_instance = new Shader$Type('VEC4', 0);
    Shader$Type$MAT4_instance = new Shader$Type('MAT4', 1);
  }
  var Shader$Type$VEC4_instance;
  function Shader$Type$VEC4_getInstance() {
    Shader$Type_initFields();
    return Shader$Type$VEC4_instance;
  }
  var Shader$Type$MAT4_instance;
  function Shader$Type$MAT4_getInstance() {
    Shader$Type_initFields();
    return Shader$Type$MAT4_instance;
  }
  Shader$Type.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Type',
    interfaces: [Enum]
  };
  function Shader$Type$values() {
    return [Shader$Type$VEC4_getInstance(), Shader$Type$MAT4_getInstance()];
  }
  Shader$Type.values = Shader$Type$values;
  function Shader$Type$valueOf(name) {
    switch (name) {
      case 'VEC4':
        return Shader$Type$VEC4_getInstance();
      case 'MAT4':
        return Shader$Type$MAT4_getInstance();
      default:throwISE('No enum constant webgl.shaders.Shader.Type.' + name);
    }
  }
  Shader$Type.valueOf_61zpoe$ = Shader$Type$valueOf;
  function Shader$program$lambda$loadShader(closure$gl) {
    return function (type, source) {
      var tmp$;
      var shader = closure$gl.createShader(type);
      closure$gl.shaderSource(shader, source);
      closure$gl.compileShader(shader);
      if (!(typeof (tmp$ = closure$gl.getShaderParameter(shader, WebGLRenderingContext.COMPILE_STATUS)) === 'boolean' ? tmp$ : throwCCE())) {
        closure$gl.deleteShader(shader);
        throw Exception_init('Error compiling shader: ' + toString(closure$gl.getShaderInfoLog(shader)));
      }
      return ensureNotNull(shader);
    };
  }
  Shader.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Shader',
    interfaces: []
  };
  function Mat4Color(gl) {
    Shader.call(this, gl, vert_getInstance().mat4_color, frag_getInstance().varying);
    this.gl = gl;
    this.a_position = this.attribute_61zpoe$('a_position').vec4();
    this.a_color = this.attribute_61zpoe$('a_color').vec4();
    this.u_matrix = this.uniform_61zpoe$('u_matrix').mat4();
  }
  Mat4Color.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Mat4Color',
    interfaces: [Shader]
  };
  function Mat4_0(gl) {
    Shader.call(this, gl, vert_getInstance().mat4, frag_getInstance().uniform);
    this.gl = gl;
    this.a_position = this.attribute_61zpoe$('a_position').vec4();
    this.u_color = this.uniform_61zpoe$('u_color').vec4();
    this.u_matrix = this.uniform_61zpoe$('u_matrix').mat4();
  }
  Mat4_0.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Mat4',
    interfaces: [Shader]
  };
  function GLX(gl) {
    this.gl = gl;
  }
  function GLX$ShaderType(name, ordinal, code) {
    Enum.call(this);
    this.code = code;
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function GLX$ShaderType_initFields() {
    GLX$ShaderType_initFields = function () {
    };
    GLX$ShaderType$Vertex_instance = new GLX$ShaderType('Vertex', 0, WebGLRenderingContext.VERTEX_SHADER);
    GLX$ShaderType$Fragment_instance = new GLX$ShaderType('Fragment', 1, WebGLRenderingContext.FRAGMENT_SHADER);
  }
  var GLX$ShaderType$Vertex_instance;
  function GLX$ShaderType$Vertex_getInstance() {
    GLX$ShaderType_initFields();
    return GLX$ShaderType$Vertex_instance;
  }
  var GLX$ShaderType$Fragment_instance;
  function GLX$ShaderType$Fragment_getInstance() {
    GLX$ShaderType_initFields();
    return GLX$ShaderType$Fragment_instance;
  }
  GLX$ShaderType.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ShaderType',
    interfaces: [Enum]
  };
  function GLX$ShaderType$values() {
    return [GLX$ShaderType$Vertex_getInstance(), GLX$ShaderType$Fragment_getInstance()];
  }
  GLX$ShaderType.values = GLX$ShaderType$values;
  function GLX$ShaderType$valueOf(name) {
    switch (name) {
      case 'Vertex':
        return GLX$ShaderType$Vertex_getInstance();
      case 'Fragment':
        return GLX$ShaderType$Fragment_getInstance();
      default:throwISE('No enum constant webgl.GLX.ShaderType.' + name);
    }
  }
  GLX$ShaderType.valueOf_61zpoe$ = GLX$ShaderType$valueOf;
  GLX.prototype.initShaderProgram_puj7f4$ = function (vertexShaderSource, fragmentShaderSource) {
    var tmp$;
    var vertexShader = this.loadShader_7sj873$(GLX$ShaderType$Vertex_getInstance(), vertexShaderSource);
    var fragmentShader = this.loadShader_7sj873$(GLX$ShaderType$Fragment_getInstance(), fragmentShaderSource);
    var shaderProgram = this.gl.createProgram();
    this.gl.attachShader(shaderProgram, vertexShader);
    this.gl.attachShader(shaderProgram, fragmentShader);
    this.gl.linkProgram(shaderProgram);
    if (!(typeof (tmp$ = this.gl.getProgramParameter(shaderProgram, WebGLRenderingContext.LINK_STATUS)) === 'boolean' ? tmp$ : throwCCE())) {
      throw Exception_init('Error: ' + toString(this.gl.getProgramInfoLog(shaderProgram)));
    }
    return new GLX$Program(this, ensureNotNull(shaderProgram));
  };
  GLX.prototype.loadShader_7sj873$ = function (type, source) {
    var tmp$;
    var shader = this.gl.createShader(type.code);
    this.gl.shaderSource(shader, source);
    this.gl.compileShader(shader);
    if (!(typeof (tmp$ = this.gl.getShaderParameter(shader, WebGLRenderingContext.COMPILE_STATUS)) === 'boolean' ? tmp$ : throwCCE())) {
      this.gl.deleteShader(shader);
      throw Exception_init('Error: ' + toString(this.gl.getShaderInfoLog(shader)));
    }
    return ensureNotNull(shader);
  };
  function GLX$Program($outer, program) {
    this.$outer = $outer;
    this.program = program;
  }
  GLX$Program.prototype.getAttribLocation_61zpoe$ = function (name) {
    return this.$outer.gl.getAttribLocation(this.program, name);
  };
  GLX$Program.prototype.getUniformLocation_61zpoe$ = function (name) {
    return this.$outer.gl.getUniformLocation(this.program, name);
  };
  GLX$Program.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Program',
    interfaces: []
  };
  GLX.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GLX',
    interfaces: []
  };
  var package$common = _.common || (_.common = {});
  var package$glsl = package$common.glsl || (package$common.glsl = {});
  Object.defineProperty(package$glsl, 'vert', {
    get: vert_getInstance
  });
  Object.defineProperty(package$glsl, 'frag', {
    get: frag_getInstance
  });
  var package$game = package$common.game || (package$common.game = {});
  package$game.FullGame_init_61zpoe$ = FullGame_init;
  package$game.FullGame = FullGame;
  package$game.Loop = Loop;
  var package$mat4 = _.mat4 || (_.mat4 = {});
  package$mat4.Mat4_init = Mat4_init;
  package$mat4.Mat4 = Mat4;
  Shader.Attribute = Shader$Attribute;
  Shader.TypedAttribute = Shader$TypedAttribute;
  Shader.Uniform = Shader$Uniform;
  Shader.TypedUniform = Shader$TypedUniform;
  Object.defineProperty(Shader$Type, 'VEC4', {
    get: Shader$Type$VEC4_getInstance
  });
  Object.defineProperty(Shader$Type, 'MAT4', {
    get: Shader$Type$MAT4_getInstance
  });
  Shader.Type = Shader$Type;
  var package$webgl = _.webgl || (_.webgl = {});
  var package$shaders = package$webgl.shaders || (package$webgl.shaders = {});
  package$shaders.Shader = Shader;
  package$shaders.Mat4Color = Mat4Color;
  package$shaders.Mat4 = Mat4_0;
  Object.defineProperty(GLX$ShaderType, 'Vertex', {
    get: GLX$ShaderType$Vertex_getInstance
  });
  Object.defineProperty(GLX$ShaderType, 'Fragment', {
    get: GLX$ShaderType$Fragment_getInstance
  });
  GLX.ShaderType = GLX$ShaderType;
  GLX.Program = GLX$Program;
  package$webgl.GLX = GLX;
  Kotlin.defineModule('common', _);
  return _;
}(typeof common === 'undefined' ? {} : common, kotlin);
