if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'main'.");
}
if (typeof common === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'common' was not found. Please, check whether 'common' is loaded prior to 'main'.");
}
if (typeof this['kotlinx-html-js'] === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'main'.");
}
var main = function (_, Kotlin, $module$common, $module$kotlinx_html_js) {
  'use strict';
  var AppOptions = $module$common.firebase.AppOptions;
  var Unit = Kotlin.kotlin.Unit;
  var initializeApp = firebase.initializeApp;
  var auth = firebase.auth;
  var AuthUI = firebaseui.auth.AuthUI;
  var Config = $module$common.firebaseui.auth.Config;
  var GoogleAuthProvider$Companion = firebase.auth.GoogleAuthProvider;
  var Callbacks = $module$common.firebaseui.auth.Callbacks;
  var ensureNotNull = Kotlin.ensureNotNull;
  var div = $module$kotlinx_html_js.kotlinx.html.js.div_wkomt5$;
  var append = $module$kotlinx_html_js.kotlinx.html.dom.append_k9bwru$;
  var throwUPAE = Kotlin.throwUPAE;
  var Kind_INTERFACE = Kotlin.Kind.INTERFACE;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var ButtonType = $module$kotlinx_html_js.kotlinx.html.ButtonType;
  var span = $module$kotlinx_html_js.kotlinx.html.js.span_x24v7w$;
  var button = $module$kotlinx_html_js.kotlinx.html.js.button_yqfwmz$;
  var a = $module$kotlinx_html_js.kotlinx.html.js.a_5i6vd$;
  var set_id = $module$kotlinx_html_js.kotlinx.html.set_id_ueiko3$;
  var li = $module$kotlinx_html_js.kotlinx.html.js.li_525bpd$;
  var ul = $module$kotlinx_html_js.kotlinx.html.js.ul_693so7$;
  var nav = $module$kotlinx_html_js.kotlinx.html.js.nav_pc7gpz$;
  var set_onClickFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onClickFunction_pszlq2$;
  var firestore = firebase.firestore;
  var Settings = $module$common.firebase.firestore.Settings;
  var get_create = $module$kotlinx_html_js.kotlinx.html.dom.get_create_4wc2mh$;
  var get_typeEnum = $module$common.firebase.firestore.get_typeEnum_samrru$;
  var insertAt = $module$common.common.insertAt_2al840$;
  var removeAt = $module$common.common.removeAt_r4ncir$;
  var replaceAt = $module$common.common.replaceAt_2al840$;
  function main$lambda$lambda() {
    window.alert('Terms of Service...');
    return Unit;
  }
  function main$lambda$lambda_0() {
    window.alert('Pirvacy Policy...');
    return Unit;
  }
  function main$lambda$lambda$lambda(f, f_0) {
    loggedin();
    return false;
  }
  function main$lambda$lambda_1($receiver) {
    return Unit;
  }
  function main$lambda(closure$authContainer) {
    return function ($receiver) {
      closure$authContainer.v = div($receiver, void 0, main$lambda$lambda_1);
      return Unit;
    };
  }
  function main(args) {
    var $receiver = AppOptions();
    $receiver.apiKey = 'AIzaSyCb6qMpHHbW6fvpHcChLg5Ah7pDPvwnl6I';
    $receiver.authDomain = 'laurafortunecom.firebaseapp.com';
    $receiver.databaseURL = 'https://laurafortunecom.firebaseio.com';
    $receiver.projectId = 'laurafortunecom';
    $receiver.storageBucket = 'laurafortunecom.appspot.com';
    $receiver.messagingSenderId = '28407257069';
    initializeApp($receiver);
    var ui = new AuthUI(auth());
    var $receiver_0 = Config();
    $receiver_0.signInOptions = [GoogleAuthProvider$Companion.PROVIDER_ID];
    $receiver_0.tosUrl = main$lambda$lambda;
    $receiver_0.privacyPolicyUrl = main$lambda$lambda_0;
    var $receiver_1 = Callbacks();
    $receiver_1.signInSuccessWithAuthResult = main$lambda$lambda$lambda;
    $receiver_0.callbacks = $receiver_1;
    var config = $receiver_0;
    var authContainer = {v: null};
    append(ensureNotNull(document.body), main$lambda(authContainer));
    ui.start(authContainer.v == null ? throwUPAE('authContainer') : authContainer.v, config);
    loggedin();
  }
  function Tab() {
  }
  Tab.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Tab',
    interfaces: []
  };
  function CategoriesTab() {
    CategoriesTab_instance = this;
  }
  CategoriesTab.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'CategoriesTab',
    interfaces: [Tab]
  };
  var CategoriesTab_instance = null;
  function CategoriesTab_getInstance() {
    if (CategoriesTab_instance === null) {
      new CategoriesTab();
    }
    return CategoriesTab_instance;
  }
  function ProductsTab() {
    ProductsTab_instance = this;
  }
  ProductsTab.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ProductsTab',
    interfaces: [Tab]
  };
  var ProductsTab_instance = null;
  function ProductsTab_getInstance() {
    if (ProductsTab_instance === null) {
      new ProductsTab();
    }
    return ProductsTab_instance;
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda(this$) {
    return function ($receiver) {
      var $receiver_0 = $receiver.attributes;
      var key = 'data-target';
      $receiver_0.put_xwzc9p$(key, '#navbar');
      var $receiver_1 = $receiver.attributes;
      var key_0 = 'data-toggle';
      var value = 'collapse';
      $receiver_1.put_xwzc9p$(key_0, value);
      var $receiver_2 = $receiver.attributes;
      var key_1 = 'aria-expanded';
      $receiver_2.put_xwzc9p$(key_1, 'false');
      var $receiver_3 = $receiver.attributes;
      var key_2 = 'aria-controls';
      $receiver_3.put_xwzc9p$(key_2, 'navbar');
      span(this$, 'icon-bar');
      span(this$, 'icon-bar');
      span(this$, 'icon-bar');
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda_0($receiver) {
    $receiver.unaryPlus_pdl1vz$('laurafortune.com Admin');
    return Unit;
  }
  function loggedin$lambda$lambda$lambda$lambda(this$) {
    return function ($receiver) {
      button(this$, void 0, void 0, void 0, ButtonType.button, 'navbar-toggle collapsed', loggedin$lambda$lambda$lambda$lambda$lambda(this$));
      a(this$, void 0, void 0, 'navbar-brand', loggedin$lambda$lambda$lambda$lambda$lambda_0);
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('Categories');
    return Unit;
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda$lambda(this$) {
    return function ($receiver) {
      a(this$, '#', void 0, void 0, loggedin$lambda$lambda$lambda$lambda$lambda$lambda$lambda);
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda$lambda$lambda_0($receiver) {
    $receiver.unaryPlus_pdl1vz$('Products');
    return Unit;
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda$lambda_0(this$) {
    return function ($receiver) {
      a(this$, '#', void 0, void 0, loggedin$lambda$lambda$lambda$lambda$lambda$lambda$lambda_0);
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda_1(this$) {
    return function ($receiver) {
      li(this$, 'active', loggedin$lambda$lambda$lambda$lambda$lambda$lambda(this$));
      li(this$, void 0, loggedin$lambda$lambda$lambda$lambda$lambda$lambda_0(this$));
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda$lambda_0(this$) {
    return function ($receiver) {
      set_id($receiver, 'navbar');
      ul(this$, 'nav navbar-nav', loggedin$lambda$lambda$lambda$lambda$lambda_1(this$));
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda(this$) {
    return function ($receiver) {
      div(this$, 'navbar-header', loggedin$lambda$lambda$lambda$lambda(this$));
      div(this$, 'navbar-collapse collapse', loggedin$lambda$lambda$lambda$lambda_0(this$));
      return Unit;
    };
  }
  function loggedin$lambda$lambda(this$) {
    return function ($receiver) {
      div(this$, 'container', loggedin$lambda$lambda$lambda(this$));
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda_2($receiver) {
    return Unit;
  }
  function loggedin$lambda$lambda$lambda$lambda_1(this$) {
    return function ($receiver) {
      span(this$, 'glyphicon glyphicon-plus', loggedin$lambda$lambda$lambda$lambda$lambda_2);
      return Unit;
    };
  }
  function loggedin$lambda$lambda$lambda$lambda$lambda_3(f) {
    return Unit;
  }
  function loggedin$lambda$lambda$lambda$lambda_2($receiver) {
    return Unit;
  }
  function loggedin$lambda$lambda$lambda_0(this$, closure$categoriesList) {
    return function ($receiver) {
      button(this$, void 0, void 0, void 0, ButtonType.button, 'btn btn-primary', loggedin$lambda$lambda$lambda$lambda_1(this$));
      set_onClickFunction($receiver, loggedin$lambda$lambda$lambda$lambda$lambda_3);
      closure$categoriesList.v = ul(this$, 'list-group', loggedin$lambda$lambda$lambda$lambda_2);
      return Unit;
    };
  }
  function loggedin$lambda$lambda_0(this$, closure$categoriesList) {
    return function ($receiver) {
      div(this$, 'starter-template', loggedin$lambda$lambda$lambda_0(this$, closure$categoriesList));
      return Unit;
    };
  }
  function loggedin$lambda(closure$categoriesList) {
    return function ($receiver) {
      nav($receiver, 'navbar navbar-inverse navbar-fixed-top', loggedin$lambda$lambda($receiver));
      div($receiver, 'container', loggedin$lambda$lambda_0($receiver, closure$categoriesList));
      return Unit;
    };
  }
  function loggedin$itemElement$lambda(closure$category) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$category.name);
      return Unit;
    };
  }
  function loggedin$itemElement(category) {
    return li(get_create(document), 'list-group-item', loggedin$itemElement$lambda(category));
  }
  function loggedin$lambda_0(closure$categoriesList, closure$itemElement) {
    return function (doc) {
      var $receiver = doc.docChanges();
      var tmp$;
      for (tmp$ = 0; tmp$ !== $receiver.length; ++tmp$) {
        var element = $receiver[tmp$];
        var closure$categoriesList_0 = closure$categoriesList;
        var closure$itemElement_0 = closure$itemElement;
        switch (get_typeEnum(element).name) {
          case 'added':
            insertAt(closure$categoriesList_0.v == null ? throwUPAE('categoriesList') : closure$categoriesList_0.v, element.newIndex, closure$itemElement_0(element.doc.data()));
            break;
          case 'removed':
            removeAt(closure$categoriesList_0.v == null ? throwUPAE('categoriesList') : closure$categoriesList_0.v, element.oldIndex);
            break;
          case 'modified':
            var ie = closure$itemElement_0(element.doc.data());
            if (element.newIndex === element.oldIndex)
              replaceAt(closure$categoriesList_0.v == null ? throwUPAE('categoriesList') : closure$categoriesList_0.v, element.oldIndex, ie);
            else {
              removeAt(closure$categoriesList_0.v == null ? throwUPAE('categoriesList') : closure$categoriesList_0.v, element.oldIndex);
              insertAt(closure$categoriesList_0.v == null ? throwUPAE('categoriesList') : closure$categoriesList_0.v, element.newIndex, ie);
            }

            break;
        }
        console.dir(element);
        console.dir(element.doc.data());
      }
      return Unit;
    };
  }
  function loggedin$lambda_1(error) {
    console.dir(error);
    return Unit;
  }
  function loggedin() {
    var categoriesList = {v: null};
    append(ensureNotNull(document.body), loggedin$lambda(categoriesList));
    var db = firestore();
    var $receiver = Settings();
    $receiver.timestampsInSnapshots = true;
    db.settings($receiver);
    var itemElement = loggedin$itemElement;
    db.collection('categories').orderBy('name').onSnapshot(loggedin$lambda_0(categoriesList, itemElement), loggedin$lambda_1);
  }
  var package$admin = _.admin || (_.admin = {});
  package$admin.main_kand9s$ = main;
  package$admin.Tab = Tab;
  Object.defineProperty(package$admin, 'CategoriesTab', {
    get: CategoriesTab_getInstance
  });
  Object.defineProperty(package$admin, 'ProductsTab', {
    get: ProductsTab_getInstance
  });
  package$admin.loggedin = loggedin;
  main([]);
  Kotlin.defineModule('main', _);
  return _;
}(typeof main === 'undefined' ? {} : main, kotlin, common, this['kotlinx-html-js']);
