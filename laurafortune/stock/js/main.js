if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'main'.");
}
if (typeof firebasekt === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'firebasekt' was not found. Please, check whether 'firebasekt' is loaded prior to 'main'.");
}
if (typeof common === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'common' was not found. Please, check whether 'common' is loaded prior to 'main'.");
}
if (typeof this['kotlinx-html-js'] === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'main'.");
}
var main = function (_, Kotlin, $module$firebasekt, $module$common, $module$kotlinx_html_js) {
  'use strict';
  var Login = $module$firebasekt.firebaseui.Login;
  var lazy = Kotlin.kotlin.lazy_klfg04$;
  var firestore = firebase.firestore;
  var Settings = $module$firebasekt.firebase.firestore.Settings;
  var Unit = Kotlin.kotlin.Unit;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var Killable = $module$common.rx.Killable;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var Var = $module$common.rx.Var;
  var getCallableRef = Kotlin.getCallableRef;
  var equals = Kotlin.equals;
  var Rx_init = $module$common.rx.Rx_init_klfg04$;
  var killOld = $module$common.rx.killOld_5kwh6e$;
  var AppOptions = $module$firebasekt.firebase.AppOptions;
  var initializeApp = firebase.initializeApp;
  var auth = firebase.auth;
  var ensureNotNull = Kotlin.ensureNotNull;
  var Slider = $module$common.common.Slider;
  var fa = $module$common.common.fa_bq9830$;
  var Screen = $module$common.common.Screen;
  var nextButton = $module$common.common.nextButton_lqo47t$;
  var rxDisplay = $module$common.common.rxDisplay_86pnez$;
  var commandButton = $module$common.common.commandButton_lqo47t$;
  var div = $module$kotlinx_html_js.kotlinx.html.js.div_wkomt5$;
  var append = $module$kotlinx_html_js.kotlinx.html.dom.append_k9bwru$;
  var Panel = $module$common.common.Panel;
  var Killables = $module$common.common.Killables;
  var hourglass = $module$common.common.hourglass_oypc8e$;
  var get_create = $module$kotlinx_html_js.kotlinx.html.dom.get_create_4wc2mh$;
  var span = $module$kotlinx_html_js.kotlinx.html.js.span_x24v7w$;
  var styles = $module$common.styles;
  var img = $module$kotlinx_html_js.kotlinx.html.js.img_6lw7hj$;
  var dt = $module$kotlinx_html_js.kotlinx.html.js.dt_iewpf2$;
  var dd = $module$kotlinx_html_js.kotlinx.html.js.dd_8uujpe$;
  var add = $module$common.rx.add_hbf74d$;
  var rxText = $module$common.common.rxText_btzx6a$;
  var dl = $module$kotlinx_html_js.kotlinx.html.js.dl_4s12uu$;
  var ButtonType = $module$kotlinx_html_js.kotlinx.html.ButtonType;
  var set_onClickFunction = $module$kotlinx_html_js.kotlinx.html.js.set_onClickFunction_pszlq2$;
  var button = $module$kotlinx_html_js.kotlinx.html.js.button_yqfwmz$;
  var listen = $module$firebasekt.firebase.firestore.listen_h20tb0$;
  var nextButton_0 = $module$common.common.nextButton_dp6e3f$;
  var insertAt = $module$common.common.insertAt_tevjyx$;
  var removeAt = $module$common.common.removeAt_poj3bi$;
  Loading.prototype = Object.create(UserStatus.prototype);
  Loading.prototype.constructor = Loading;
  Guest.prototype = Object.create(UserStatus.prototype);
  Guest.prototype.constructor = Guest;
  LoggedIn.prototype = Object.create(UserStatus.prototype);
  LoggedIn.prototype.constructor = LoggedIn;
  function login$lambda() {
    return new Login();
  }
  var login;
  function get_login() {
    return login.value;
  }
  function db$lambda() {
    var db = firestore();
    var $receiver = Settings();
    $receiver.timestampsInSnapshots = true;
    db.settings($receiver);
    return db;
  }
  var db;
  function get_db() {
    return db.value;
  }
  function baseRef$lambda() {
    return get_db().collection('projects').doc('stock');
  }
  var baseRef;
  function get_baseRef() {
    return baseRef.value;
  }
  function UserStatus() {
  }
  UserStatus.prototype.kill = function () {
  };
  UserStatus.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'UserStatus',
    interfaces: [Killable]
  };
  function Loading() {
    Loading_instance = this;
    UserStatus.call(this);
  }
  Loading.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Loading',
    interfaces: [UserStatus]
  };
  var Loading_instance = null;
  function Loading_getInstance() {
    if (Loading_instance === null) {
      new Loading();
    }
    return Loading_instance;
  }
  function Guest() {
    Guest_instance = this;
    UserStatus.call(this);
  }
  Guest.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Guest',
    interfaces: [UserStatus]
  };
  var Guest_instance = null;
  function Guest_getInstance() {
    if (Guest_instance === null) {
      new Guest();
    }
    return Guest_instance;
  }
  function LoggedIn(user, afterFirstSnapshot) {
    UserStatus.call(this);
    this.user = user;
    this.configVar_0 = new Var(null);
    this.snapshotHandler_0 = LoggedIn$snapshotHandler$lambda(this, afterFirstSnapshot);
    this.killUpdates_0 = get_baseRef().collection('users').doc(this.user.uid).onSnapshot(LoggedIn$killUpdates$lambda(this));
  }
  Object.defineProperty(LoggedIn.prototype, 'config', {
    get: function () {
      return this.configVar_0;
    }
  });
  LoggedIn.prototype.updateConfig_5t1gnd$ = function (snapshot) {
    if (!snapshot.exists) {
      this.configVar_0.now = null;
    }
     else {
      this.configVar_0.now = snapshot.data();
    }
  };
  LoggedIn.prototype.kill = function () {
    this.killUpdates_0();
    UserStatus.prototype.kill.call(this);
  };
  function LoggedIn$snapshotHandler$lambda(this$LoggedIn, closure$afterFirstSnapshot) {
    return function (it) {
      this$LoggedIn.updateConfig_5t1gnd$(it);
      this$LoggedIn.snapshotHandler_0 = getCallableRef('updateConfig', function ($receiver, snapshot) {
        return $receiver.updateConfig_5t1gnd$(snapshot), Unit;
      }.bind(null, this$LoggedIn));
      closure$afterFirstSnapshot(this$LoggedIn);
      return Unit;
    };
  }
  function LoggedIn$killUpdates$lambda(this$LoggedIn) {
    return function (it) {
      this$LoggedIn.snapshotHandler_0(it);
      return Unit;
    };
  }
  LoggedIn.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'LoggedIn',
    interfaces: [UserStatus]
  };
  function Privileges(status) {
    this.status_0 = status;
    this.config_0 = Rx_init(Privileges$config$lambda(this));
    this.admin = Rx_init(Privileges$admin$lambda(this));
    this.categories = Rx_init(Privileges$categories$lambda(this));
  }
  function Privileges$config$lambda(this$Privileges) {
    return function () {
      var s = this$Privileges.status_0.invoke();
      if (equals(s, Loading_getInstance()) || equals(s, Guest_getInstance()))
        return null;
      else if (Kotlin.isType(s, LoggedIn))
        return s.config.invoke();
      else
        return Kotlin.noWhenBranchMatched();
    };
  }
  function Privileges$admin$lambda(this$Privileges) {
    return function () {
      var tmp$, tmp$_0;
      return (tmp$_0 = (tmp$ = this$Privileges.config_0.invoke()) != null ? tmp$.admin : null) != null ? tmp$_0 : false;
    };
  }
  function Privileges$categories$lambda(this$Privileges) {
    return function () {
      return this$Privileges.admin.invoke();
    };
  }
  Privileges.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Privileges',
    interfaces: []
  };
  var userStatus;
  var privileges;
  function main$lambda$lambda($receiver) {
    userStatus.now = $receiver;
    return Unit;
  }
  function main$lambda(user) {
    if (user != null) {
      new LoggedIn(user, main$lambda$lambda);
    }
     else {
      userStatus.now = Guest_getInstance();
    }
    return Unit;
  }
  function main(args) {
    var $receiver = AppOptions();
    $receiver.apiKey = 'AIzaSyCb6qMpHHbW6fvpHcChLg5Ah7pDPvwnl6I';
    $receiver.authDomain = 'laurafortunecom.firebaseapp.com';
    $receiver.databaseURL = 'https://laurafortunecom.firebaseio.com';
    $receiver.projectId = 'laurafortunecom';
    $receiver.storageBucket = 'laurafortunecom.appspot.com';
    $receiver.messagingSenderId = '28407257069';
    initializeApp($receiver);
    auth().onAuthStateChanged(main$lambda);
    new Slider(mainMenu(), ensureNotNull(document.body));
  }
  function mainMenu$lambda($receiver) {
    fa($receiver, 'home');
    return Unit;
  }
  function mainMenu$lambda_0($receiver, wrapper) {
    new Main(wrapper, $receiver);
    return Unit;
  }
  function mainMenu() {
    return new Screen(mainMenu$lambda, mainMenu$lambda_0);
  }
  function UserContext() {
    this.user = new Var(null);
    this.config = new Var(null);
  }
  UserContext.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'UserContext',
    interfaces: []
  };
  function Main(wrapper, tc) {
    this.wrapper = wrapper;
    this.rootPanel_0 = new Panel(div(tc, 'w-100 h-100'));
    this.killables_0 = new Killables();
    userStatus.forEach_qlkmfe$(Main_init$lambda(this)).addTo_g9f6u0$(this.killables_0);
  }
  function Main$loggedIn$lambda$lambda$lambda$lambda(this$Main) {
    return function () {
      this$Main.wrapper.slider().next_znjm9$(categories());
      return Unit;
    };
  }
  function Main$loggedIn$lambda$lambda$lambda$lambda_0(this$Main, closure$log) {
    return function () {
      this$Main.wrapper.slider().next_znjm9$(userProfile(closure$log));
      return Unit;
    };
  }
  function Main$loggedIn$lambda$lambda$lambda$lambda_1() {
    auth().signOut();
    return Unit;
  }
  function Main$loggedIn$lambda$lambda$lambda(this$Main, this$, this$_0, closure$log) {
    return function ($receiver) {
      rxDisplay(nextButton(this$, 'Categories', Main$loggedIn$lambda$lambda$lambda$lambda(this$Main)), privileges.categories).addTo_g9f6u0$(this$_0.killables);
      nextButton(this$, 'User Profile', Main$loggedIn$lambda$lambda$lambda$lambda_0(this$Main, closure$log));
      commandButton(this$, 'Sign Out', Main$loggedIn$lambda$lambda$lambda$lambda_1);
      return Unit;
    };
  }
  function Main$loggedIn$lambda$lambda(this$Main, this$, closure$log) {
    return function ($receiver) {
      div($receiver, 'list-group list-group-flush', Main$loggedIn$lambda$lambda$lambda(this$Main, $receiver, this$, closure$log));
      return Unit;
    };
  }
  function Main$loggedIn$lambda$lambda_0(this$Main) {
    return function () {
      this$Main.wrapper.focus();
      this$Main.wrapper.disposeNexts();
      return Unit;
    };
  }
  Main.prototype.loggedIn_m9j54x$ = function (log) {
    var $receiver = this.rootPanel_0.new_lt8gi4$();
    append($receiver.tab, Main$loggedIn$lambda$lambda(this, $receiver, log));
    $receiver.killables.add_o14v8n$(Main$loggedIn$lambda$lambda_0(this));
  };
  function Main_init$lambda$lambda($receiver) {
    hourglass($receiver);
    return Unit;
  }
  function Main_init$lambda(this$Main) {
    return function (it) {
      if (equals(it, Loading_getInstance()))
        append(this$Main.rootPanel_0.new_lt8gi4$().tab, Main_init$lambda$lambda);
      else if (equals(it, Guest_getInstance())) {
        var authElement = this$Main.rootPanel_0.new_lt8gi4$(div(get_create(document), 'p-2')).tab;
        get_login().loginUi_2rdptt$(authElement);
      }
       else if (Kotlin.isType(it, LoggedIn))
        this$Main.loggedIn_m9j54x$(it);
      else
        Kotlin.noWhenBranchMatched();
      return Unit;
    };
  }
  Main.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Main',
    interfaces: []
  };
  function userProfile$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('User Profile');
    return Unit;
  }
  function userProfile$lambda($receiver) {
    span($receiver, void 0, userProfile$lambda$lambda);
    return Unit;
  }
  function userProfile$lambda$lambda$lambda$lambda(closure$log, this$) {
    return function ($receiver) {
      var tmp$;
      if (closure$log.user.photoURL != null) {
        tmp$ = closure$log.user.photoURL;
        img(this$, void 0, tmp$, 'mw-100 border rounded p-2');
      }
      return Unit;
    };
  }
  function userProfile$lambda$lambda$lambda$lambda$item$lambda(closure$label) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$label);
      return Unit;
    };
  }
  function userProfile$lambda$lambda$lambda$lambda$item$lambda_0(closure$value) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$value);
      return Unit;
    };
  }
  function userProfile$lambda$lambda$lambda$lambda$item(this$) {
    return function (label, value) {
      if (value != null) {
        dt(this$, void 0, userProfile$lambda$lambda$lambda$lambda$item$lambda(label));
        dd(this$, void 0, userProfile$lambda$lambda$lambda$lambda$item$lambda_0(value));
      }
    };
  }
  function userProfile$lambda$lambda$lambda$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('Admin');
    return Unit;
  }
  function userProfile$lambda$lambda$lambda$lambda$lambda_0() {
    return privileges.admin.invoke() ? 'Yes' : 'No';
  }
  function userProfile$lambda$lambda$lambda$lambda_0(this$, closure$log, closure$wrapper) {
    return function ($receiver) {
      var item = userProfile$lambda$lambda$lambda$lambda$item(this$);
      item('Name', closure$log.user.displayName);
      item('Email', closure$log.user.email);
      dt(this$, void 0, userProfile$lambda$lambda$lambda$lambda$lambda);
      var tmp$ = dd(this$);
      var $receiver_0 = Rx_init(userProfile$lambda$lambda$lambda$lambda$lambda_0);
      add(closure$wrapper.killables, $receiver_0);
      rxText(tmp$, $receiver_0);
      return Unit;
    };
  }
  function userProfile$lambda$lambda$lambda(closure$log, this$, closure$wrapper) {
    return function ($receiver) {
      div(this$, 'd-flex w-100 flex-column align-items-center', userProfile$lambda$lambda$lambda$lambda(closure$log, this$));
      dl(this$, void 0, userProfile$lambda$lambda$lambda$lambda_0(this$, closure$log, closure$wrapper));
      return Unit;
    };
  }
  function userProfile$lambda$lambda$lambda$lambda_1($receiver) {
    $receiver.unaryPlus_pdl1vz$('Back');
    return Unit;
  }
  function userProfile$lambda$lambda$lambda$lambda_2(closure$wrapper) {
    return function (it) {
      var $receiver = ensureNotNull(closure$wrapper.prev);
      $receiver.focus();
      $receiver.disposeNexts();
      return Unit;
    };
  }
  function userProfile$lambda$lambda$lambda_0(this$, closure$wrapper) {
    return function ($receiver) {
      fa(this$, 'chevron-left');
      span(this$, 'flex-grow-1', userProfile$lambda$lambda$lambda$lambda_1);
      set_onClickFunction($receiver, userProfile$lambda$lambda$lambda$lambda_2(closure$wrapper));
      return Unit;
    };
  }
  function userProfile$lambda$lambda_0(closure$log, this$, closure$wrapper) {
    return function ($receiver) {
      div(this$, 'flex-grow-1 m-2 ' + styles.scrollVertical, userProfile$lambda$lambda$lambda(closure$log, this$, closure$wrapper));
      button(this$, void 0, void 0, void 0, ButtonType.button, 'btn btn-light border flex-shrink-0 d-flex flex-row m-2', userProfile$lambda$lambda$lambda_0(this$, closure$wrapper));
      return Unit;
    };
  }
  function userProfile$lambda_0(closure$log) {
    return function ($receiver, wrapper) {
      div($receiver, 'd-flex flex-column w-100 h-100', userProfile$lambda$lambda_0(closure$log, $receiver, wrapper));
      return Unit;
    };
  }
  function userProfile(log) {
    return new Screen(userProfile$lambda, userProfile$lambda_0(log));
  }
  function categories$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('Categories');
    return Unit;
  }
  function categories$lambda($receiver) {
    span($receiver, void 0, categories$lambda$lambda);
    return Unit;
  }
  function categories$lambda$lambda$lambda(closure$panel, closure$list) {
    return function () {
      closure$panel.new_lt8gi4$(closure$list);
      return Unit;
    };
  }
  function categories$lambda$lambda$lambda$lambda$lambda(closure$v) {
    return function () {
      return closure$v.invoke().name;
    };
  }
  function categories$lambda$lambda$lambda$lambda(closure$v) {
    return function ($receiver) {
      rxText(span($receiver), Rx_init(categories$lambda$lambda$lambda$lambda$lambda(closure$v)));
      return Unit;
    };
  }
  function categories$lambda$lambda$lambda$lambda_0() {
    return Unit;
  }
  function categories$lambda$lambda$lambda_0(closure$list) {
    return function (at, v) {
      insertAt(closure$list, at, nextButton_0(get_create(document), categories$lambda$lambda$lambda$lambda(v), categories$lambda$lambda$lambda$lambda_0));
      return Unit;
    };
  }
  function categories$lambda$lambda$lambda_1(closure$list) {
    return function (it) {
      removeAt(closure$list, it);
      return Unit;
    };
  }
  function categories$lambda$lambda$lambda_2(closure$list) {
    return function (from, to) {
      insertAt(closure$list, to, removeAt(closure$list, from));
      return Unit;
    };
  }
  function categories$lambda$lambda_0(this$, closure$wrapper) {
    return function ($receiver) {
      var $receiver_0 = new Panel(div(this$, 'w-100 h-100'));
      $receiver_0.new_lt8gi4$(hourglass(get_create(document)));
      var panel = $receiver_0;
      var list = div(get_create(document), 'list-group list-group-flush border-bottom');
      var result = listen(get_baseRef().collection('categories'), closure$wrapper.killables, categories$lambda$lambda$lambda(panel, list));
      result.addListener_6r1g8a$(categories$lambda$lambda$lambda_0(list), categories$lambda$lambda$lambda_1(list), categories$lambda$lambda$lambda_2(list));
      return Unit;
    };
  }
  function categories$lambda_0($receiver, wrapper) {
    wrapper.backPanel_xfb89v$($receiver, categories$lambda$lambda_0($receiver, wrapper));
    return Unit;
  }
  function categories() {
    return new Screen(categories$lambda, categories$lambda_0);
  }
  var package$stock = _.stock || (_.stock = {});
  Object.defineProperty(package$stock, 'login', {
    get: get_login
  });
  Object.defineProperty(package$stock, 'db', {
    get: get_db
  });
  Object.defineProperty(package$stock, 'baseRef', {
    get: get_baseRef
  });
  package$stock.UserStatus = UserStatus;
  Object.defineProperty(package$stock, 'Loading', {
    get: Loading_getInstance
  });
  Object.defineProperty(package$stock, 'Guest', {
    get: Guest_getInstance
  });
  package$stock.LoggedIn = LoggedIn;
  package$stock.Privileges = Privileges;
  Object.defineProperty(package$stock, 'userStatus', {
    get: function () {
      return userStatus;
    }
  });
  Object.defineProperty(package$stock, 'privileges', {
    get: function () {
      return privileges;
    }
  });
  package$stock.main_kand9s$ = main;
  package$stock.mainMenu = mainMenu;
  package$stock.UserContext = UserContext;
  package$stock.Main = Main;
  package$stock.userProfile_m9j54x$ = userProfile;
  package$stock.categories = categories;
  UserStatus.prototype.addTo_g9f6u0$ = Killable.prototype.addTo_g9f6u0$;
  login = lazy(login$lambda);
  db = lazy(db$lambda);
  baseRef = lazy(baseRef$lambda);
  var $receiver = new Var(Loading_getInstance());
  killOld($receiver);
  userStatus = $receiver;
  privileges = new Privileges(userStatus);
  main([]);
  Kotlin.defineModule('main', _);
  return _;
}(typeof main === 'undefined' ? {} : main, kotlin, firebasekt, common, this['kotlinx-html-js']);
