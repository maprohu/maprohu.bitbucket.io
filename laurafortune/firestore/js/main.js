if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'main'.");
}
if (typeof common === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'common' was not found. Please, check whether 'common' is loaded prior to 'main'.");
}
if (typeof this['kotlinx-html-js'] === 'undefined') {
  throw new Error("Error loading module 'main'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'main'.");
}
var main = function (_, Kotlin, $module$common, $module$kotlinx_html_js) {
  'use strict';
  var obj = $module$common.common.obj_287e2$;
  var Var = $module$common.rx.Var;
  var Rx_init = $module$common.rx.Rx_init_klfg04$;
  var throwUPAE = Kotlin.throwUPAE;
  var safeLoad = jsyaml.safeLoad;
  var Annotation = $module$common.ace.Annotation;
  var Unit = Kotlin.kotlin.Unit;
  var YAMLException = jsyaml.YAMLException;
  var div = $module$kotlinx_html_js.kotlinx.html.js.div_wkomt5$;
  var menuitem = $module$common.common.menuitem_16yqb9$;
  var attachEnabler = $module$common.common.attachEnabler_2mgs05$;
  var dropdown = $module$common.common.dropdown_xt65r4$;
  var edit = ace.edit;
  var AppOptions = $module$common.firebase.AppOptions;
  var initializeApp = firebase.initializeApp;
  var auth = firebase.auth;
  var get_create = $module$kotlinx_html_js.kotlinx.html.dom.get_create_4wc2mh$;
  var Config = $module$common.firebaseui.auth.Config;
  var GoogleAuthProvider$Companion = firebase.auth.GoogleAuthProvider;
  var EmailAuthProvider$Companion = firebase.auth.EmailAuthProvider;
  var Listeners = $module$common.common.Listeners;
  var ol = $module$kotlinx_html_js.kotlinx.html.js.ol_qmgqht$;
  var throwCCE = Kotlin.throwCCE;
  var li = $module$kotlinx_html_js.kotlinx.html.js.li_525bpd$;
  var append = $module$kotlinx_html_js.kotlinx.html.dom.append_k9bwru$;
  var ul = $module$kotlinx_html_js.kotlinx.html.js.ul_693so7$;
  var ensureNotNull = Kotlin.ensureNotNull;
  var Panel = $module$common.common.Panel;
  var firestore = firebase.firestore;
  var Settings = $module$common.firebase.firestore.Settings;
  var lazy = Kotlin.kotlin.lazy_klfg04$;
  var AuthUI = firebaseui.auth.AuthUI;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  function UserConfig() {
    return obj();
  }
  function editConfig$lambda(closure$updating) {
    return function () {
      return !closure$updating.invoke();
    };
  }
  function editConfig$validate(closure$editor) {
    return function (goto) {
      try {
        safeLoad((closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).getSession().getValue());
        (closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).getSession().clearAnnotations();
      }
       catch (e) {
        if (Kotlin.isType(e, YAMLException)) {
          var tmp$ = (closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).getSession();
          var $receiver = Annotation();
          var closure$e = e;
          $receiver.row = closure$e.mark.line;
          $receiver.column = closure$e.mark.column;
          $receiver.text = closure$e.reason;
          $receiver.type = 'error';
          tmp$.setAnnotations([$receiver]);
          if (goto) {
            (closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).focus();
            (closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).gotoLine(e.mark.line + 1 | 0, e.mark.column + 1 | 0, true);
          }
        }
         else
          throw e;
      }
    };
  }
  function editConfig$save(this$editConfig, closure$editor) {
    return function () {
      var tmp$ = this$editConfig.userConfigRef;
      var $receiver = UserConfig();
      var closure$editor_0 = closure$editor;
      $receiver.scheme = (closure$editor_0.v == null ? throwUPAE('editor') : closure$editor_0.v).getSession().getValue();
      return tmp$.update($receiver);
    };
  }
  function editConfig$lambda$lambda$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('Edit Config');
    return Unit;
  }
  function editConfig$lambda$lambda$lambda$lambda$lambda(closure$validate) {
    return function (it) {
      closure$validate(true);
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda$lambda$lambda_0(closure$save) {
    return function (it) {
      closure$save();
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda$lambda$lambda$lambda(this$editConfig) {
    return function (it) {
      this$editConfig.loggedIn_8be2vx$();
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda$lambda$lambda_1(closure$updating, closure$save, this$editConfig) {
    return function (it) {
      if (!closure$updating.now) {
        closure$updating.now = true;
        closure$save().then(editConfig$lambda$lambda$lambda$lambda$lambda$lambda(this$editConfig));
      }
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda$lambda$lambda_2(closure$userConfig, this$editConfig) {
    return function (it) {
      this$editConfig.loggedInLoaded_lsrhdl$(closure$userConfig);
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda$lambda_0(closure$validate, this$, closure$enabled, closure$save, closure$updating, this$editConfig, closure$userConfig) {
    return function ($receiver) {
      var $receiver_0 = menuitem(this$, 'Validate', editConfig$lambda$lambda$lambda$lambda$lambda(closure$validate));
      attachEnabler($receiver_0, closure$enabled);
      menuitem(this$, 'Save', editConfig$lambda$lambda$lambda$lambda$lambda_0(closure$save));
      var $receiver_1 = menuitem(this$, 'Submit', editConfig$lambda$lambda$lambda$lambda$lambda_1(closure$updating, closure$save, this$editConfig));
      attachEnabler($receiver_1, closure$enabled);
      menuitem(this$, 'Cancel', editConfig$lambda$lambda$lambda$lambda$lambda_2(closure$userConfig, this$editConfig));
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda(this$, closure$validate, closure$enabled, closure$save, closure$updating, this$editConfig, closure$userConfig) {
    return function ($receiver) {
      div(this$, 'flex-grow-1', editConfig$lambda$lambda$lambda$lambda);
      dropdown(this$, editConfig$lambda$lambda$lambda$lambda_0(closure$validate, this$, closure$enabled, closure$save, closure$updating, this$editConfig, closure$userConfig));
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda$lambda_1(closure$validate) {
    return function () {
      closure$validate(false);
      return Unit;
    };
  }
  function editConfig$lambda$lambda$lambda_0(closure$currentTimeout, closure$validate) {
    return function () {
      var tmp$;
      if ((tmp$ = closure$currentTimeout.v) != null) {
        window.clearTimeout(tmp$);
      }
      window.setTimeout(editConfig$lambda$lambda$lambda$lambda_1(closure$validate), 2000);
      return Unit;
    };
  }
  function editConfig$lambda$lambda(this$, closure$validate, closure$enabled, closure$save, closure$updating, this$editConfig, closure$userConfig, closure$editor) {
    return function ($receiver) {
      div(this$, 'border-bottom bg-light d-flex flex-row align-items-center p-1 pl-3', editConfig$lambda$lambda$lambda(this$, closure$validate, closure$enabled, closure$save, closure$updating, this$editConfig, closure$userConfig));
      closure$editor.v = edit(div(this$, 'flex-grow-1'));
      var session = (closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).getSession();
      session.setMode('ace/mode/yaml');
      session.setValue(closure$userConfig.scheme);
      session.setTabSize(2);
      session.setUseSoftTabs(true);
      (closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).focus();
      closure$validate(false);
      var currentTimeout = {v: null};
      (closure$editor.v == null ? throwUPAE('editor') : closure$editor.v).on('change', editConfig$lambda$lambda$lambda_0(currentTimeout, closure$validate));
      return Unit;
    };
  }
  function editConfig$lambda_0(closure$validate, closure$enabled, closure$save, closure$updating, this$editConfig, closure$userConfig, closure$editor) {
    return function ($receiver) {
      div($receiver, 'w-100 h-100 d-flex flex-column', editConfig$lambda$lambda($receiver, closure$validate, closure$enabled, closure$save, closure$updating, this$editConfig, closure$userConfig, closure$editor));
      return Unit;
    };
  }
  function editConfig($receiver, userConfig) {
    var editor = {v: null};
    var updating = new Var(false);
    var enabled = Rx_init(editConfig$lambda(updating));
    var validate = editConfig$validate(editor);
    var save = editConfig$save($receiver, editor);
    $receiver.rootPanel.build_388moq$(editConfig$lambda_0(validate, enabled, save, updating, $receiver, userConfig, editor));
  }
  function main(args) {
    (new Main()).run();
  }
  function Main() {
    this.rootPanel = new Panel(ensureNotNull(document.body));
    this.db_rj70gk$_0 = lazy(Main$db$lambda);
    this.userConfigRef_8yz478$_0 = lazy(Main$userConfigRef$lambda(this));
    this.ui_rj7d8e$_0 = lazy(Main$ui$lambda);
  }
  Object.defineProperty(Main.prototype, 'db', {
    get: function () {
      return this.db_rj70gk$_0.value;
    }
  });
  Object.defineProperty(Main.prototype, 'userConfigRef', {
    get: function () {
      return this.userConfigRef_8yz478$_0.value;
    }
  });
  function Main$run$lambda(this$Main) {
    return function (user) {
      if (user != null) {
        this$Main.loggedIn_8be2vx$();
      }
       else {
        this$Main.loginUi_0();
      }
      return Unit;
    };
  }
  Main.prototype.run = function () {
    var $receiver = AppOptions();
    $receiver.apiKey = 'AIzaSyCb6qMpHHbW6fvpHcChLg5Ah7pDPvwnl6I';
    $receiver.authDomain = 'laurafortunecom.firebaseapp.com';
    $receiver.databaseURL = 'https://laurafortunecom.firebaseio.com';
    $receiver.projectId = 'laurafortunecom';
    $receiver.storageBucket = 'laurafortunecom.appspot.com';
    $receiver.messagingSenderId = '28407257069';
    initializeApp($receiver);
    auth().onAuthStateChanged(Main$run$lambda(this));
  };
  Object.defineProperty(Main.prototype, 'ui_0', {
    get: function () {
      return this.ui_rj7d8e$_0.value;
    }
  });
  function Main$loginUi$lambda$lambda() {
    window.alert('Terms of Service...');
    return Unit;
  }
  function Main$loginUi$lambda$lambda_0() {
    window.alert('Privacy Policy...');
    return Unit;
  }
  Main.prototype.loginUi_0 = function () {
    var authElement = this.rootPanel.set_a4pgib$(div(get_create(document)));
    var $receiver = Config();
    $receiver.signInFlow = 'popup';
    $receiver.signInOptions = [GoogleAuthProvider$Companion.PROVIDER_ID, EmailAuthProvider$Companion.PROVIDER_ID];
    $receiver.tosUrl = Main$loginUi$lambda$lambda;
    $receiver.privacyPolicyUrl = Main$loginUi$lambda$lambda_0;
    var config = $receiver;
    this.ui_0.start(authElement, config);
  };
  function Main$loggedIn$lambda(this$Main) {
    return function (it) {
      this$Main.loggedInLoaded_lsrhdl$(it.data());
      return Unit;
    };
  }
  Main.prototype.loggedIn_8be2vx$ = function () {
    this.userConfigRef.get().then(Main$loggedIn$lambda(this));
  };
  function Main$loggedInLoaded$lambda() {
    auth().signOut();
    return Unit;
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda(closure$userConfig, this$Main) {
    return function (it) {
      editConfig(this$Main, closure$userConfig);
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda_0(closure$logout) {
    return function (it) {
      closure$logout.fire();
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda(closure$userConfig, this$Main, this$, closure$logout) {
    return function ($receiver) {
      menuitem(this$, 'Edit Config', Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda(closure$userConfig, this$Main));
      menuitem(this$, 'Logout', Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda_0(closure$logout));
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda(this$, closure$breadCrumb, closure$userConfig, this$Main, closure$logout) {
    return function ($receiver) {
      closure$breadCrumb.v = ol(this$, 'breadcrumb no-round mb-0 grow bg-transparent');
      dropdown(this$, Main$loggedInLoaded$lambda$lambda$lambda$lambda(closure$userConfig, this$Main, this$, closure$logout));
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda_0($receiver) {
    return Unit;
  }
  function Main$loggedInLoaded$lambda$lambda(this$, closure$breadCrumb, closure$userConfig, this$Main, closure$logout, closure$contentPanel) {
    return function ($receiver) {
      div(this$, 'd-flex flex-row align-items-center border-bottom bg-light pr-1', Main$loggedInLoaded$lambda$lambda$lambda(this$, closure$breadCrumb, closure$userConfig, this$Main, closure$logout));
      closure$contentPanel.v = div(this$, 'grow horizontal', Main$loggedInLoaded$lambda$lambda$lambda_0);
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda_0(closure$breadCrumb, closure$userConfig, this$Main, closure$logout, closure$contentPanel) {
    return function ($receiver) {
      div($receiver, 'full d-flex flex-column', Main$loggedInLoaded$lambda$lambda($receiver, closure$breadCrumb, closure$userConfig, this$Main, closure$logout, closure$contentPanel));
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda_0(closure$config) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$config.label);
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda_1(closure$config) {
    return function ($receiver) {
      li($receiver, 'breadcrumb-item active', Main$loggedInLoaded$lambda$lambda_0(closure$config));
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda_0(closure$config) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$config.label);
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda_1(it) {
    return Unit;
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda_1(this$) {
    return function ($receiver) {
      menuitem(this$, 'Do Something', Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda_1);
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda_1(closure$config, this$) {
    return function ($receiver) {
      div(this$, 'grow', Main$loggedInLoaded$lambda$lambda$lambda$lambda_0(closure$config));
      dropdown(this$, Main$loggedInLoaded$lambda$lambda$lambda$lambda_1(this$));
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda$lambda(closure$it) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$(closure$it.label);
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda$lambda_2(closure$config, this$) {
    return function ($receiver) {
      var $receiver_0 = closure$config.items;
      var tmp$;
      for (tmp$ = 0; tmp$ !== $receiver_0.length; ++tmp$) {
        var element = $receiver_0[tmp$];
        li(this$, 'btn btn-light list-group-item text-left', Main$loggedInLoaded$lambda$lambda$lambda$lambda$lambda$lambda(element));
      }
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda$lambda_2(closure$config, this$) {
    return function ($receiver) {
      ul(this$, 'list-group list-group-flush', Main$loggedInLoaded$lambda$lambda$lambda$lambda_2(closure$config, this$));
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda_1(closure$config, this$) {
    return function ($receiver) {
      div(this$, 'border-bottom bg-light d-flex flex-row align-items-center p-1 pl-3', Main$loggedInLoaded$lambda$lambda$lambda_1(closure$config, this$));
      div(this$, void 0, Main$loggedInLoaded$lambda$lambda$lambda_2(closure$config, this$));
      return Unit;
    };
  }
  function Main$loggedInLoaded$lambda$lambda_2($receiver) {
    return Unit;
  }
  function Main$loggedInLoaded$lambda$lambda_3($receiver) {
    return Unit;
  }
  function Main$loggedInLoaded$lambda_2(closure$config) {
    return function ($receiver) {
      div($receiver, 'border-right grow-2 vertical', Main$loggedInLoaded$lambda$lambda_1(closure$config, $receiver));
      div($receiver, 'border-right grow-2', Main$loggedInLoaded$lambda$lambda_2);
      div($receiver, 'grow-3', Main$loggedInLoaded$lambda$lambda_3);
      return Unit;
    };
  }
  Main.prototype.loggedInLoaded_lsrhdl$ = function (userConfig) {
    var tmp$;
    var breadCrumb = {v: null};
    var contentPanel = {v: null};
    var logout = new Listeners();
    logout.add_o14v8n$(Main$loggedInLoaded$lambda);
    this.rootPanel.build_388moq$(Main$loggedInLoaded$lambda_0(breadCrumb, userConfig, this, logout, contentPanel));
    var config = Kotlin.isType(tmp$ = safeLoad(userConfig.scheme), Object) ? tmp$ : throwCCE();
    append(breadCrumb.v == null ? throwUPAE('breadCrumb') : breadCrumb.v, Main$loggedInLoaded$lambda_1(config));
    append(contentPanel.v == null ? throwUPAE('contentPanel') : contentPanel.v, Main$loggedInLoaded$lambda_2(config));
  };
  function Main$db$lambda() {
    var db = firestore();
    var $receiver = Settings();
    $receiver.timestampsInSnapshots = true;
    db.settings($receiver);
    return db;
  }
  function Main$userConfigRef$lambda(this$Main) {
    return function () {
      return this$Main.db.collection('userconfig').doc(ensureNotNull(auth().currentUser).uid);
    };
  }
  function Main$ui$lambda() {
    return new AuthUI(auth());
  }
  Main.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Main',
    interfaces: []
  };
  var package$fsui = _.fsui || (_.fsui = {});
  var package$domain = package$fsui.domain || (package$fsui.domain = {});
  package$domain.UserConfig = UserConfig;
  package$fsui.editConfig_q9lghc$ = editConfig;
  package$fsui.main_kand9s$ = main;
  package$fsui.Main = Main;
  main([]);
  Kotlin.defineModule('main', _);
  return _;
}(typeof main === 'undefined' ? {} : main, kotlin, common, this['kotlinx-html-js']);
