if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'sandbox'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'sandbox'.");
}
if (typeof common === 'undefined') {
  throw new Error("Error loading module 'sandbox'. Its dependency 'common' was not found. Please, check whether 'common' is loaded prior to 'sandbox'.");
}
var sandbox = function (_, Kotlin, $module$common) {
  'use strict';
  var trimIndent = Kotlin.kotlin.text.trimIndent_pdl1vz$;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var FullGame_init = $module$common.common.game.FullGame_init_61zpoe$;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var FullGame = $module$common.common.game.FullGame;
  Sandbox.prototype = Object.create(FullGame.prototype);
  Sandbox.prototype.constructor = Sandbox;
  function Glsl() {
    Glsl_instance = this;
    this.fragment = trimIndent('\nprecision mediump float;\n\nvarying vec2 v_loc;\nuniform float u_yOffset;\n\nvoid main() {\n\n    gl_FragColor = vec4(\n        v_loc.x * 0.5 + 0.5,\n        cos((-u_yOffset * 0.2 + length(v_loc)) * 5.0),\n        (cos((u_yOffset + v_loc.y) * 10.0) * 0.5 + 0.5) * (cos((u_yOffset + v_loc.x) * 5.0) * 0.5 + 0.5),\n        1.0\n    );\n\n}\n\n');
    this.vertex = trimIndent('\nattribute vec4 aVertexPosition;\n\nvarying vec2 v_loc;\n\nvoid main() {\n  gl_Position = aVertexPosition;\n  v_loc = vec2(aVertexPosition.x, aVertexPosition.y);\n}\n');
  }
  Glsl.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Glsl',
    interfaces: []
  };
  var Glsl_instance = null;
  function Glsl_getInstance() {
    if (Glsl_instance === null) {
      new Glsl();
    }
    return Glsl_instance;
  }
  function main(args) {
    (new Sandbox()).run();
  }
  function Sandbox() {
    FullGame_init(void 0, this);
    var program = this.glx.initShaderProgram_puj7f4$(Glsl_getInstance().vertex, Glsl_getInstance().fragment);
    var vertexPositionLoc = program.getAttribLocation_61zpoe$('aVertexPosition');
    var positionBuffer = this.gl.createBuffer();
    this.gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, positionBuffer);
    var positions = new Float32Array([-1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0]);
    this.gl.bufferData(WebGLRenderingContext.ARRAY_BUFFER, positions, WebGLRenderingContext.STATIC_DRAW);
    var numComponents = 2;
    var type = WebGLRenderingContext.FLOAT;
    var normalize = false;
    var stride = 0;
    var offset = 0;
    this.gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, positionBuffer);
    this.gl.vertexAttribPointer(vertexPositionLoc, numComponents, type, normalize, stride, offset);
    this.gl.enableVertexAttribArray(vertexPositionLoc);
    this.program = program;
    this.yOffsetLoc = this.program.getUniformLocation_61zpoe$('u_yOffset');
  }
  Sandbox.prototype.step = function () {
  };
  Sandbox.prototype.draw = function () {
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
    this.gl.clear(WebGLRenderingContext.COLOR_BUFFER_BIT);
    this.gl.useProgram(this.program.program);
    this.gl.uniform1f(this.yOffsetLoc, this.lastFrameTs / 1000.0);
    var vertexOffset = 0;
    var vertexCount = 4;
    this.gl.drawArrays(WebGLRenderingContext.TRIANGLE_STRIP, vertexOffset, vertexCount);
  };
  Sandbox.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Sandbox',
    interfaces: [FullGame]
  };
  var package$glsl = _.glsl || (_.glsl = {});
  Object.defineProperty(package$glsl, 'Glsl', {
    get: Glsl_getInstance
  });
  var package$sandbox = _.sandbox || (_.sandbox = {});
  package$sandbox.main_kand9s$ = main;
  package$sandbox.Sandbox = Sandbox;
  main([]);
  Kotlin.defineModule('sandbox', _);
  return _;
}(typeof sandbox === 'undefined' ? {} : sandbox, kotlin, common);
