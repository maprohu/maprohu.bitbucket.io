if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'balloons'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'balloons'.");
}
if (typeof common === 'undefined') {
  throw new Error("Error loading module 'balloons'. Its dependency 'common' was not found. Please, check whether 'common' is loaded prior to 'balloons'.");
}
var balloons = function (_, Kotlin, $module$common) {
  'use strict';
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var FullGame_init = $module$common.common.game.FullGame_init_61zpoe$;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var FullGame = $module$common.common.game.FullGame;
  Balloons.prototype = Object.create(FullGame.prototype);
  Balloons.prototype.constructor = Balloons;
  function vert() {
    vert_instance = this;
  }
  vert.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'vert',
    interfaces: []
  };
  var vert_instance = null;
  function vert_getInstance() {
    if (vert_instance === null) {
      new vert();
    }
    return vert_instance;
  }
  function frag() {
    frag_instance = this;
  }
  frag.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'frag',
    interfaces: []
  };
  var frag_instance = null;
  function frag_getInstance() {
    if (frag_instance === null) {
      new frag();
    }
    return frag_instance;
  }
  function main(args) {
    (new Balloons()).run();
  }
  function Balloons() {
    FullGame_init(void 0, this);
  }
  Balloons.prototype.draw = function () {
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
    this.gl.clear(WebGLRenderingContext.COLOR_BUFFER_BIT);
  };
  Balloons.prototype.step = function () {
  };
  Balloons.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Balloons',
    interfaces: [FullGame]
  };
  var package$balloons = _.balloons || (_.balloons = {});
  var package$glsl = package$balloons.glsl || (package$balloons.glsl = {});
  Object.defineProperty(package$glsl, 'vert', {
    get: vert_getInstance
  });
  Object.defineProperty(package$glsl, 'frag', {
    get: frag_getInstance
  });
  package$balloons.main_kand9s$ = main;
  package$balloons.Balloons = Balloons;
  main([]);
  Kotlin.defineModule('balloons', _);
  return _;
}(typeof balloons === 'undefined' ? {} : balloons, kotlin, common);
